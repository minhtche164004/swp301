/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.NewsGroup;

/**
 *
 * @author ADMIN
 */
public class NewsGroupDAO {

    static Connection cnn; // kết nối
    static PreparedStatement stm; // thực hiên các cáu lệnh sql
    static ResultSet rs; // lưu trữ và xử lí dữ liệu

    public static NewsGroup getNewsGroupbyID(int newsgroupID) {
        NewsGroup c = new NewsGroup();
        try {
            String sql = "select [newsgroup_id]\n"
                    + "      ,[parent_id]\n"
                    + "      ,[code]\n"
                    + "      ,[newsgroup_name]\n"
                    + "      ,[url]\n"
                    + "      ,[status]\n"
                    + "      ,[created_at]\n"
                    + "      ,[created_by]\n"
                    + "      ,[modified_at]\n"
                    + "      ,[modified_by]\n"
                    + "from [NewsGroups] where newsgroup_id=" + newsgroupID + "";
            cnn = DBConnect.getConnection();
            stm = cnn.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                c.setNewsgroup_id(rs.getInt(1));
                c.setParent_id(rs.getInt(2));
                c.setCode(rs.getString(3));
                c.setNewsgroup_name(rs.getString(4));
                c.setUrl(rs.getString(5));
                c.setStatus(rs.getBoolean(6));
                c.setCreated_at(rs.getDate(7));
                c.setCreated_by(rs.getInt(8));
                c.setModified_at(rs.getDate(9));
                c.setModified_by(rs.getInt(10));
            }
        } catch (Exception e) {
            System.out.println("getNewsGroupbyID error: " + e.getMessage());
        }
        return c;
    }

    public static ArrayList<NewsGroup> getlistNotNullNewsGroup() {
        ArrayList<NewsGroup> ng = new ArrayList<>();
        try {
            String sql = "select [newsgroup_id]\n"
                    + "      ,[parent_id]\n"
                    + "      ,[code]\n"
                    + "      ,[newsgroup_name]\n"
                    + "      ,[url]\n"
                    + "      ,[status]\n"
                    + "      ,[created_at]\n"
                    + "      ,[created_by]\n"
                    + "      ,[modified_at]\n"
                    + "      ,[modified_by]\n"
                    + "from [NewsGroups] where parent_id is not null";
            cnn = DBConnect.getConnection();
            stm = cnn.prepareStatement(sql);
            rs = stm.executeQuery();
            while (rs.next()) {
                NewsGroup c = new NewsGroup();
                c.setNewsgroup_id(rs.getInt(1));
                c.setParent_id(rs.getInt(2));
                c.setCode(rs.getString(3));
                c.setNewsgroup_name(rs.getString(4));
                c.setUrl(rs.getString(5));
                c.setStatus(rs.getBoolean(6));
                c.setCreated_at(rs.getDate(7));
                c.setCreated_by(rs.getInt(8));
                c.setModified_at(rs.getDate(9));
                c.setModified_by(rs.getInt(10));
                ng.add(c);
            }
        } catch (Exception e) {
            System.out.println("getNewsGroupbyID error: " + e.getMessage());
        }
        return ng;
    }
}
