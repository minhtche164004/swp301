/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller.Payment;

import dal.OrderDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import model.Order;

/**
 *
 * @author TNA
 */

@WebServlet(name = "vnpayReturn", value = "/vnpay-return")
public class vnpayReturn extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter out = resp.getWriter();
        try {
            /*  IPN URL: Record payment results from VNPAY
                        Implementation steps:
                        Check checksum
                        Find transactions (vnp_TxnRef) in the database (checkOrderId)
                        Check the payment status of transactions before updating (checkOrderStatus)
                        Check the amount (vnp_Amount) of transactions before updating (checkAmount)
                        Update results to Database
                        Return recorded results to VNPAY
             */
            // ex:  	PaymnentStatus = 0; pending 
            //              PaymnentStatus = 1; success 
            //              PaymnentStatus = 2; Faile 
            //Begin process return from VNPAY
            Map fields = new HashMap();
            for (Enumeration params = req.getParameterNames(); params.hasMoreElements();) {
                String fieldName = URLEncoder.encode((String) params.nextElement(), StandardCharsets.US_ASCII.toString());
                String fieldValue = URLEncoder.encode(req.getParameter(fieldName), StandardCharsets.US_ASCII.toString());
                if ((fieldValue != null) && (fieldValue.length() > 0)) {
                    fields.put(fieldName, fieldValue);
                }
            }

            String vnp_SecureHash = req.getParameter("vnp_SecureHash");
            if (fields.containsKey("vnp_SecureHashType")) {
                fields.remove("vnp_SecureHashType");
            }
            if (fields.containsKey("vnp_SecureHash")) {
                fields.remove("vnp_SecureHash");
            }
            
            //check sum
            String signValue = Config.hashAllFields(fields);            
            if (signValue.equals(vnp_SecureHash)) {
                
                String orderCode = req.getParameter("vnp_TxnRef");
                Order o = OrderDAO.getOrderByCode(orderCode);
                double amount = Double.parseDouble(req.getParameter("vnp_Amount"));

                boolean checkOrderId = (o != null); // vnp_TxnRef exists in your database
                if (checkOrderId) {
                    boolean checkAmount = (amount == o.getTotal()*100); // vnp_Amount is valid (Check vnp_Amount VNPAY returns compared to the amount of the code (vnp_TxnRef) in the Your database).
                    if (checkAmount) {
                        boolean checkOrderStatus = (!o.isPayment()); // PaymnentStatus = 0 (pending)
                        if (checkOrderStatus) {
                            if ("00".equals(req.getParameter("vnp_ResponseCode"))) {
                                o.setPayment(true);
                                OrderDAO.updateOrder(o);
                                req.setAttribute("mess", "Payment is successful, the product will be delivered to you in the nearest time!!");
                                //Here Code update PaymnentStatus = 1 into your Database
                            } else {
                                req.setAttribute("mess", "Payment failed, please pay again!!");
                                // Here Code update PaymnentStatus = 2 into your Database
                            }
                            out.print("{\"RspCode\":\"00\",\"Message\":\"Confirm Success\"}");
                        } else {
                            req.setAttribute("mess", "Order already confirmed!!");
                            out.print("{\"RspCode\":\"02\",\"Message\":\"Order already confirmed\"}");
                        }
                    } else {
                        req.setAttribute("mess", "Invalid Amount!!");
                        out.print("{\"RspCode\":\"04\",\"Message\":\"Invalid Amount\"}");
                    }
                } else {
                    req.setAttribute("mess", "Order not Found!!");                    
                    out.print("{\"RspCode\":\"01\",\"Message\":\"Order not Found\"}");
                }
            } else {
                req.setAttribute("mess", "Invalid Checksum!!");                                    
                out.print("{\"RspCode\":\"97\",\"Message\":\"Invalid Checksum\"}");
            }
        } catch (Exception e) {
            req.setAttribute("mess", "Unknow error!!");                                                
            out.print("{\"RspCode\":\"99\",\"Message\":\"Unknow error\"}");
        } finally {
            req.getRequestDispatcher("FrontEnd/vnpay_return.jsp").forward(req, resp);
        }
    }

}
